---
title: "Fish Stoichiometry"
author: "Jeferson Amaral and Andrés López-Sepulcre"
date: "2024-02-17"
output: html_document
---

```{r setup, include=FALSE}
library(knitr)
  opts_chunk$set(echo = TRUE)
library(tidyverse)
library(ggeffects)
library(patchwork)
library(lme4)
library(lmerTest)
library(glmmTMB)
library(sjPlot)
```

### Aim

To test whether fish from High Urbanization sites have higher nutrient stoichiometries than their Low Urbanization counterparts.

```{r}
data_gup <- read.csv("../data/AllFish.csv") %>%
  filter(Spp == 'Poecilia_reticulata', Preserved_in == "Dry", Sex == "F") %>%
  mutate(Treatment = relevel(factor(Treatment), ref = "LU")) %>% 
  rename(Somatic_.Dry_.Mass_g = Somatic_g.Dry_.Mass_Wto_Brain)
data_gup<- data_gup[-1001,]

```


```{r}
plot_urb <- function(model, ylabel){
  
  pred <- ggpredict(model, terms = c("Treatment", "River"), type = 'random')

  ggplot(pred, aes(x = x, y = predicted, group = group)) +
  geom_point(size = 4, aes(shape = group),
             position = position_dodge(width = 0.5)) +
  geom_errorbar(aes(ymin = conf.low, ymax = conf.high), 
                width = 0.1,
                position = position_dodge(width = 0.5)) +
  geom_line(position = position_dodge(width = 0.5)) +
  xlab("") + ylab(ylabel) +
  scale_x_discrete(labels=c("LU" = "Low \n Urbanization",
                            "HU" = "High \n Urbanization")) +
  theme_bw() +
  theme(panel.grid.major = element_blank(),
        panel.grid.minor = element_blank(),
        legend.position = 'bottom',
        legend.title = element_blank())
}
```

### C:N

```{r}
model_CN1 <- glmmTMB::glmmTMB(log(C.N) ~ log(Somatic_.Dry_.Mass_g) + Treatment * River, 
                 data = data_gup)
model_CN2 <- glmmTMB::glmmTMB(log(C.N) ~ log(Somatic_.Dry_.Mass_g) + Treatment + (1|River), 
                 data = data_gup) 
model_CN3 <- glmmTMB::glmmTMB(log(C.N) ~ log(Somatic_.Dry_.Mass_g) + Treatment + (Treatment|River), 
                 data = data_gup) 

anova(model_CN1, model_CN2, model_CN3)

summary(model_CN1)

emmeans::emmeans(model_CN1, ~ Treatment * River, type = "response")

```

```{r}
plot_CN <- plot_urb(model_CN1, "C:N") +
  geom_point(data =data_gup, 
             aes(x = Treatment, y = C.N, group = River),
                  position =  position_dodge(width=0.5),
             colour = 'gray', size = 5, shape = "_") +
  labs(title = "A")
```

### C:P

```{r}
model_CP1 <- glmmTMB::glmmTMB(log(C.P) ~ log(Somatic_.Dry_.Mass_g) + Treatment * River, 
                 data = data_gup)
model_CP2 <- glmmTMB::glmmTMB(log(C.P) ~ log(Somatic_.Dry_.Mass_g) + Treatment + (1|River), 
                 data = data_gup) 
model_CP3 <- glmmTMB::glmmTMB(log(C.P) ~ log(Somatic_.Dry_.Mass_g) + Treatment + (Treatment|River), 
                 data = data_gup) 

anova(model_CP1, model_CP2, model_CP3)

summary(model_CP1)

emmeans::emmeans(model_CP1, ~ Treatment * River, type = "response")

```

```{r}
plot_CP <- plot_urb(model_CP1, "C:P") +  geom_point(data =data_gup, 
             aes(x = Treatment, y = C.P, group = River),
                  position =  position_dodge(width=0.5),
             colour = 'gray', size = 5, shape = "_") +
  labs(title = "B")

```

### N:P

```{r}
model_NP1 <- glmmTMB(log(N.P) ~ log(Somatic_.Dry_.Mass_g) + Treatment * River, 
                 data = data_gup)
model_NP2 <- glmmTMB(log(N.P) ~ log(Somatic_.Dry_.Mass_g) + Treatment + (1|River), 
                 data = data_gup) 
model_NP3 <- glmmTMB(log(N.P) ~ log(Somatic_.Dry_.Mass_g) + Treatment + (Treatment|River), 
                 data = data_gup) 

anova(model_NP1, model_NP2, model_NP3)

summary(model_NP1)

emmeans::emmeans(model_NP1, ~ Treatment * River, type = "response")

```

```{r}
plot_NP <- plot_urb(model_NP1, "N:P") +
    geom_point(data =data_gup, 
             aes(x = Treatment, y = N.P, group = River),
                  position =  position_dodge(width=0.5),
             colour = 'gray', size = 5, shape = "_") +
  labs(title = "C")

```

### Joint Plots

```{r}
plot_stoich <- plot_CN + plot_CP + plot_NP &
  theme(legend.position = "bottom") 
plot_stoich + plot_layout(guides = "collect")
```


### Summary table of models for C:N, C:P, N:P

```{r}
tab_model(model_CN1, model_CP1, model_NP1,
          show.se = TRUE, 
          show.ci = FALSE, 
          collapse.se = TRUE,
          show.icc = FALSE,
          show.r2 = FALSE,
          digits = 3,
          dv.labels = c("log(C:N)", "log(C:P)", "log(N:P)"))
```


### Total C, P and N

```{r}
model_C <- glmmTMB(PerCent_C ~ Somatic_.Dry_.Mass_g + Treatment * River , data = data_gup)
model_N <- glmmTMB(PerCent_N ~ Somatic_.Dry_.Mass_g + Treatment * River, data = data_gup)
model_P <- glmmTMB(PerCent_P ~ Somatic_.Dry_.Mass_g + Treatment * River, data = data_gup)

emmeans::emmeans(model_C, ~ Treatment * River, type = "response")
emmeans::emmeans(model_P, ~ Treatment * River, type = "response")



```

```{r}
plot_C <- plot_urb(model_C, "%C") +
  geom_point(data =data_gup, 
             aes(x = Treatment, y = PerCent_C, group = River),
                  position =  position_dodge(width=0.5),
             colour = 'gray', size = 5, shape = "_")+
  labs(title = "A")

plot_N <- plot_urb(model_N, "%N") +
  geom_point(data =data_gup, 
             aes(x = Treatment, y = PerCent_N, group = River),
                  position =  position_dodge(width=0.5),
             colour = 'gray', size = 5, shape = "_")+
  labs(title = "B")

plot_P <- plot_urb(model_P, "%P") +
  geom_point(data =data_gup, 
             aes(x = Treatment, y = PerCent_P, group = River),
                  position =  position_dodge(width=0.5),
             colour = 'gray', size = 5, shape = "_")+
  labs(title = "C")

plot_stoich2 <- plot_C + plot_N + plot_P &
  theme(legend.position = "bottom") 
plot_stoich2 + plot_layout(guides = "collect")

```

### Summary table of models for %C, %N, and %P

```{r}
tab_model(model_C, model_N, model_P,
          show.se = TRUE, 
          show.ci = FALSE, 
          collapse.se = TRUE,
          show.icc = FALSE,
          show.r2 = FALSE,
          digits = 3,
          dv.labels = c("%C", "%N", "%P"))
```

