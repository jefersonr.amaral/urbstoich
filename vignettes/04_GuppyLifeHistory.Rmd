---
title: "Guppy's Life History"
author: "Jeferson and Andrés López Sepulcre"
date: "2024-05-22"
output: html_document
---

In this vignette, we are going to analyze the number of embryos, offspring weight, and reproductive allotment of fish from Maracas, Tacarigua, and Tunapuna rivers preserved in formalin (F0).  

```{r setup, include=FALSE}
library(knitr)
opts_chunk$set(echo = TRUE)
library(tidyverse)
library(ggeffects)
library(ggpubr)
library(patchwork)
library(lme4)
library(lmerTest)
library(glmmTMB)
library(sjPlot)
```


```{r}
LifeHist <- read.csv("../data/AllFish.csv") %>%
  filter(Spp == "Poecilia_reticulata", Preserved_in == "Formalin", Sex == "F") %>%
  mutate(Treatment = relevel(factor(Treatment), ref = "LU")) %>% 
  mutate(Final_Dry_Mass_mg = Final_Dry_Mass_g*1000,
         Embryo_.Dry_.Mass_mg = Embryo_.Dry_.Mass_g*1000,
         Individual_.Embryo_.dry_.Mass_mg = Individual_.Embryo_.dry_.Mass_g*1000)  
LifeHist<- LifeHist[-c(178:199,282:297),]
LifeHist$River <- as.factor(LifeHist$River)
```

Checking the total number and proportion of pregnant fish out of the analyzed ones.
```{r}
pregnant_counts <- LifeHist %>%
  group_by(River, Treatment) %>%
  summarise(
    PregnantFemales = sum(Numb_.Of_.Embs > 0, na.rm = TRUE), 
    TotalFemales = n(),                                      
    ProportionPregnant = PregnantFemales / TotalFemales      
  )
print(pregnant_counts)
```

Calculating minimum size of reproducing females by River and Treatment
```{r}
min_reproducing_sizes <- LifeHist %>%
  group_by(River, Treatment, Length_.mm) %>%
  summarise(
    Total_Females = n(),
    Reproducing_Females = sum(Numb_.Of_.Embs > 0, na.rm = TRUE),
    Proportion_Reproducing = Reproducing_Females / Total_Females,
    .groups = "drop"
  ) %>%
  arrange(River, Treatment, Length_.mm) %>%
  group_by(River, Treatment) %>%
  filter(Proportion_Reproducing > 0.5) %>%
  slice(1) %>%
  ungroup() %>%
  select(River, Treatment, Minimum_Size = Length_.mm)
print(min_reproducing_sizes)
```

Calculating the Reproductive Allotment (RA; Reznick et al. (1996)): 

$$
RA = \frac{\text{dry weight of embryos}}{\text{somatic dry weight} + \text{dry weight of embryos}}
$$

Filtering females with embryos 
```{r}
LifeHistPreg <- LifeHist %>% 
  filter(Numb_.Of_.Embs > 0)
```

Calculating RA
```{r}
LifeHistPreg<- LifeHistPreg%>% 
  mutate(RA = Embryo_.Dry_.Mass_mg/
           (Final_Dry_Mass_mg+Embryo_.Dry_.Mass_mg))
```

Plotting raw data for Number of Embryos, Offspring Weight (g), and Reproductive Allotment (%). 
```{r}
RawNumbEmb<- ggplot(LifeHistPreg, aes(x = Treatment, y = Numb_.Of_.Embs)) +
  geom_boxplot()+
 stat_summary(fun=mean, geom="point", shape=1, size = 3, aes(shape = River), 
              position = position_dodge(width = 0.7)) +
  scale_y_continuous(name = "Number of Embryos") +
    theme_bw() +
  theme(axis.text.x=element_blank(),
        axis.title.x = element_blank())+
facet_wrap(.~ River)


RawEmbWeight<- ggplot(LifeHistPreg, aes(x = Treatment, y = Individual_.Embryo_.dry_.Mass_mg)) +
  geom_boxplot()+
 stat_summary(fun=mean, geom="point", shape=1, size = 3, aes(shape = River), 
              position = position_dodge(width = 0.7)) +
  scale_y_continuous(name = "Offspring Dry Mass (mg)") +
    theme_bw() +
  theme(axis.text.x=element_blank(),
        axis.title.x = element_blank(),
        strip.background = element_blank(),
        strip.text.x = element_blank())+
  facet_wrap(.~ River)

RawRA<- ggplot(LifeHistPreg, aes(x = Treatment, y = RA*100)) +
  geom_boxplot()+
 stat_summary(fun=mean, geom="point", shape=1, size = 3, aes(shape = River), 
              position = position_dodge(width = 0.7)) +
  scale_y_continuous(name = "Reproductive Allotment (%)") +
    theme_bw() +
    theme(strip.background = element_blank(),
        strip.text.x = element_blank())+
  facet_wrap(.~ River)
  

ggarrange(RawNumbEmb, RawEmbWeight, RawRA,
          common.legend = TRUE, legend = "bottom",
          labels = c("A", "B", "C"),
          ncol = 1, nrow = 3, 
          align = "v")

```

Comparing Number of Embryos between Treatments and Rivers
```{r}
mod_Numb1<- glmmTMB(Numb_.Of_.Embs~Final_Dry_Mass_mg+
                          River*Treatment,
                        family = nbinom1(),
                        data = LifeHistPreg)
plot(DHARMa::simulateResiduals(mod_Numb1))

mod_Numb2<- glmmTMB(Numb_.Of_.Embs~Final_Dry_Mass_mg+
                          River+Treatment,
                        family = nbinom1(),
                        data = LifeHistPreg)
plot(DHARMa::simulateResiduals(mod_Numb2))

anova(mod_Numb1, mod_Numb2)

summary(mod_Numb1)

```

Comparing Offspring Weight (g) between Treatments and Rivers
```{r}
mod_OffSize1<- glmmTMB(Individual_.Embryo_.dry_.Mass_mg~
                         Final_Dry_Mass_mg+Stage+River*Treatment,
                        data = LifeHistPreg)
plot(DHARMa::simulateResiduals(mod_OffSize1))

summary(mod_OffSize1)
```

Comparing Offspring Weight (g) between Treatments and Rivers including females dry mass and the stage of development of embryos.
```{r}
mod_RA1<- glmmTMB(RA~Final_Dry_Mass_mg+Stage+River*Treatment,
                  family = beta_family(),
                        data = LifeHistPreg)
plot(DHARMa::simulateResiduals(mod_RA1))

mod_RA2<- glmmTMB(RA~Final_Dry_Mass_mg+Stage+River+Treatment,
                  family = beta_family(),
                        data = LifeHistPreg)
plot(DHARMa::simulateResiduals(mod_RA1))

anova(mod_RA1, mod_RA2)

summary(mod_RA1)
```


Comparing Offspring Weight (g) between Treatments and Rivers including only females dry mass
```{r}
mod_RA3<- glmmTMB(RA~Final_Dry_Mass_mg+River*Treatment,
                  family = beta_family(),
                        data = LifeHistPreg)
plot(DHARMa::simulateResiduals(mod_RA3))

mod_RA4<- glmmTMB(RA~Final_Dry_Mass_mg+River+Treatment,
                  family = beta_family(),
                        data = LifeHistPreg)
plot(DHARMa::simulateResiduals(mod_RA4))

anova(mod_RA3, mod_RA4)

summary(mod_RA3)
```